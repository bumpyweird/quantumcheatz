from pathlib import Path

from tkinter import Tk, Canvas, Entry, Text, Button, PhotoImage
from PIL import Image, ImageTk
import ctypes


OUTPUT_PATH = Path(__file__).parent
ASSETS_PATH = OUTPUT_PATH / Path("./assets")


def relative_to_assets(path: str) -> Path:
    return ASSETS_PATH / Path(path)


window = Tk()

window.geometry("909x568")
window.configure(bg = "#FFFFFF")
window.title("QuantumCheatz - Premium CS:GO Cheats")

def changeColor2():
    button_2.configure(bg="green")

def changeColor3():
    button_3.configure(bg="green")

def changeColor4():
    button_4.configure(bg="green")

def changeColor5():
    button_5.configure(bg="green")

def changeColor6():
    button_6.configure(bg="green")

def changeColor7():
    button_7.configure(bg="green")

def changeColor8():
    button_8.configure(bg="green")

def changeColor9():
    button_9.configure(bg="green")

def inject():
    ctypes.windll.user32.MessageBoxW(0, "[csgo.exe] - Successfully Injected!", "QuantumCheatz Injector", 1)

canvas = Canvas(
    window,
    bg = "#FFFFFF",
    height = 568,
    width = 909,
    bd = 0,
    highlightthickness = 0,
    relief = "ridge"
)

canvas.place(x = 0, y = 0)
image_image_1 = ImageTk.PhotoImage(Image.open("assets/image_1.png"))
image_1 = canvas.create_image(
    454.0,
    284.0,
    image=image_image_1
)

button_image_1 = ImageTk.PhotoImage(Image.open("assets/button_1.png"))
button_1 = Button(
    image=button_image_1,
    borderwidth=0,
    highlightthickness=0,
    command=lambda: inject(),
    relief="flat",
)
button_1.place(
    x=508.0,
    y=424.0,
    width=319.0,
    height=76.0
)

canvas.create_text(
    93.00000000000001,
    11.0,
    anchor="nw",
    text="QuantumCheatz",
    fill="#FFFFFF",
    font=("Anton Regular", 72 * -1)
)

button_image_2 = ImageTk.PhotoImage(Image.open("assets/button_2.png"))
button_2 = Button(
    image=button_image_2,
    borderwidth=0,
    highlightthickness=0,
    command=lambda: changeColor2(),
    relief="flat", bg = "white"
)
button_2.place(
    x=455.0,
    y=278.0,
    width=238.0,
    height=48.0
)

button_image_3 = ImageTk.PhotoImage(Image.open("assets/button_3.png"))
button_3 = Button(
    image=button_image_3,
    borderwidth=0,
    highlightthickness=0,
    command=lambda: changeColor3(),
    relief="flat", bg = "white"
)
button_3.place(
    x=455.0,
    y=213.0,
    width=213.0,
    height=48.0
)

button_image_4 = ImageTk.PhotoImage(Image.open("assets/button_4.png"))
button_4 = Button(
    image=button_image_4,
    borderwidth=0,
    highlightthickness=0,
    command=lambda: changeColor4(),
    relief="flat", bg = "white"
)
button_4.place(
    x=455.0,
    y=148.0,
    width=253.0,
    height=48.0
)

button_image_5 = ImageTk.PhotoImage(Image.open("assets/button_5.png"))
button_5 = Button(
    image=button_image_5,
    borderwidth=0,
    highlightthickness=0,
    command=lambda: changeColor5(),
    relief="flat", bg = "white"
)
button_5.place(
    x=71.00000000000001,
    y=414.0,
    width=176.0,
    height=48.0
)

button_image_6 = ImageTk.PhotoImage(Image.open("assets/button_6.png"))
button_6 = Button(
    image=button_image_6,
    borderwidth=0,
    highlightthickness=0,
    command=lambda: changeColor6(),
    relief="flat", bg = "white"
)
button_6.place(
    x=71.00000000000001,
    y=349.0,
    width=233.0,
    height=48.0
)

button_image_7 = ImageTk.PhotoImage(Image.open("assets/button_7.png"))
button_7 = Button(
    image=button_image_7,
    borderwidth=0,
    highlightthickness=0,
    command=lambda: changeColor7(),
    relief="flat", bg = "white"
)
button_7.place(
    x=71.00000000000001,
    y=284.0,
    width=222.0,
    height=48.0
)

button_image_8 = ImageTk.PhotoImage(Image.open("assets/button_8.png"))
button_8 = Button(
    image=button_image_8,
    borderwidth=0,
    highlightthickness=0,
    command=lambda: changeColor8(),
    relief="flat", bg = "white"
)
button_8.place(
    x=71.00000000000001,
    y=219.0,
    width=204.0,
    height=48.0
)

button_image_9 = ImageTk.PhotoImage(Image.open("assets/button_9.png"))
button_9 = Button(
    image=button_image_9,
    borderwidth=0,
    highlightthickness=0,
    command=lambda: changeColor9(),
    relief="flat", bg = "white"
)
button_9.place(
    x=71.00000000000001,
    y=154.0,
    width=176.0,
    height=48.0
)

def startGui():
    window.resizable(False, False)
    window.mainloop()
