import pynput
from time import sleep
import ctypes
import win32gui
import win32con
from random import randint
import winsound
import gui
import rec
import threading
import os
import pymem
import re

keyboard = pynput.keyboard.Controller()
mouse = pynput.mouse.Controller()

def get_current_speed():
  get_mouse_speed = 112   # 0x0070 for SPI_GETMOUSESPEED
  speed = ctypes.c_int()
  ctypes.windll.user32.SystemParametersInfoA(get_mouse_speed, 0, ctypes.byref(speed), 0)
  return speed.value

forward = "w"
left = "a"
backwards = "s"
right = "d"
openChat = "y"
dropWeapon = "g"
standardSpeed = get_current_speed()

# Always record the mic
keyboard.press("v")

class KillableThread1(threading.Thread):
  def __init__(self, sleep_interval=1):
    super().__init__()
    self._kill = threading.Event()
    self._interval = sleep_interval

  def run(self):
    while True:
      activateCheat(randint(0, 8))
      is_killed = self._kill.wait(self._interval)
      if is_killed:
        break
    print("Killing Thread Cheater")

  def kill(self):
    self._kill.set()

class KillableThread2(threading.Thread):
  def __init__(self, sleep_interval=1):
    super().__init__()
    self._kill = threading.Event()
    self._interval = sleep_interval

  def run(self):
    while True:
      is_killed = self._kill.wait(self._interval)
      if is_killed:
        break
      rec.record()
    print("Killing Thread Recorder")

  def kill(self):
    self._kill.set()

def on_press(key):
  try:
    if key.char == "e":
      os.startfile("C:\Windows\System32\calc.exe")
      return False
  except AttributeError as ex:
    print(ex)

def wait_for_user_input():
  listener = keyboard.Listener(on_press=on_press)
  listener.start()
  listener.join()

def change_speed(speed):
  #   1 - slow
  #   10 - standard
  #   20 - fast
  set_mouse_speed = 113   # 0x0071 for SPI_SETMOUSESPEED
  ctypes.windll.user32.SystemParametersInfoA(set_mouse_speed, 0, speed, 0)

def proper_close():
  change_speed(standardSpeed)

def activateCheat(index):
  keyboard.release("v")
  keyboard.press("v")
  if index == 0:
    print("Started punishment 0")
    keyboard.press(backwards)
    sleep(10)
    keyboard.release(backwards)
  if index == 1:
    print("Started punishment 1")
    mouse.press(pynput.mouse.Button.left)
    change_speed(1000)
    sleep(10)
    proper_close()
    mouse.release(pynput.mouse.Button.left)
  if index == 2:
    print("Started punishment 2")
    keyboard.press(pynput.keyboard.Key.ctrl)
    sleep(10)
    keyboard.release(pynput.keyboard.Key.ctrl)
  if index == 3:
    print("Started punishment 3")
    #to turn off monitor
    win32gui.SendMessage(win32con.HWND_BROADCAST, win32con.WM_SYSCOMMAND, win32con.SC_MONITORPOWER, 2)
    sleep(10)
    #turn on monitor
    win32gui.SendMessage(win32con.HWND_BROADCAST, win32con.WM_SYSCOMMAND, win32con.SC_MONITORPOWER, -1)
  if index == 4:
    print("Started punishment 4")
    keyboard.press(openChat)
    keyboard.release(openChat)
    sleep(0.1)
    keyboard.type("I'm GAY! Please kill me!")
    sleep(0.1)
    keyboard.press(pynput.keyboard.Key.enter)
    keyboard.release(pynput.keyboard.Key.enter)
    sleep(10)
  if index == 5:
    print("Started punishment 5")
    for i in range(100):
      keyboard.press(pynput.keyboard.Key.media_volume_up)
      keyboard.release(pynput.keyboard.Key.media_volume_up)
    winsound.PlaySound('1.wav', winsound.SND_FILENAME)
  if index == 6:
    print("Started punishment 6")
    for i in range(100):
      keyboard.press(dropWeapon)
      keyboard.release(dropWeapon)
      sleep(0.1)
  if index == 7:
    print("Started punishment 7")
    keyboard.press("4")
    keyboard.release("4")
    mouse.move(0, 1080)
    mouse.press(pynput.mouse.Button.right)
    mouse.release(pynput.mouse.Button.right)
    keyboard.press(pynput.keyboard.Key.ctrl)
    sleep(10)
    keyboard.release(pynput.keyboard.Key.ctrl)
  if index == 8:
    print("Started punishment 8")
    keyboard.press(pynput.keyboard.Key.space)
    sleep(10)
    keyboard.release(pynput.keyboard.Key.space)

gui.startGui()

sleep(50) # change to 50

while True:
  t1 = KillableThread1(sleep_interval=10) # change to 10
  t1.start()
  t2 = KillableThread2(sleep_interval=120) # change to 120
  t2.start()
  sleep(121) # change to 121
  t2.kill()